Provides:org.mozilla.firefox
Categories:Internet
License:MPL2
Web Site:http://www.gnu.org/software/gnuzilla/
Source Code:http://hg.mozilla.org
Issue Tracker:https://bugzilla.mozilla.org
Donate:https://my.fsf.org/donate/

Name:IceCatMobile
Summary:Web browser
Description:
Browser using the Gecko layout engine to render web pages, which implements
current and anticipated web standards. This is a free software rebranding of the
latest Firefox-ESR release.

Anti-features: Non-free Addons: The license of the addons/modules on
addons.mozilla.org may be seen in the version notes of the addon and often the
license is Custom or other non-free. However there is no such license info for
most apps from the marketplace.
.

Repo Type:hg
Repo:https://hg.mozilla.org/releases/mozilla-esr38

Build:38.6.0,380600
    commit=FIREFOX_38_6_0esr_RELEASE
    output=obj-android/dist/gecko-unsigned-unaligned.apk
    srclibs=MozLocales@96b78c77,IceCat@d75da834
    prebuild=fxarch=arm-linux-androideabi && \
        bash $$IceCat$$/makeicecat --skip-compare-locales --skip-l10n --no-debian --no-tarball --with-sourcedir=$PWD && \
        bash $$MozLocales$$/prebuild-icecat.sh  && \
        echo "ac_add_options --with-android-ndk=$$NDK$$" >> .mozconfig && \
        echo "ac_add_options --with-android-sdk=$$SDK$$/platforms/android-22" >> .mozconfig && \
        echo "ac_add_options --target=${fxarch}" >> .mozconfig && \
        echo "mk_add_options 'export ANDROID_VERSION_CODE=$$VERCODE$$'" >> .mozconfig
    scanignore=mobile/android/base/JavaAddonManager.java
    build=./mach build && \
        pushd obj-android/mobile/android/locales && \
        for loc in $(cat ../../../../used-locales); do LOCALE_MERGEDIR=$PWD/merge-$loc make merge-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; make LOCALE_MERGEDIR=$PWD/merge-$loc chrome-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; done && \
        popd && \
        ./mach package

Build:38.7.1,380701
    commit=FIREFOX_38_7_1esr_RELEASE
    output=obj-android/dist/gecko-unsigned-unaligned.apk
    srclibs=MozLocales@96b78c77,IceCat@f6c6629a
    prebuild=fxarch=`echo $$VERCODE$$ | cut -c 5 | sed -e 's/0/arm-linux-androideabi/g' -e 's/1/i386-linux-androideabi/g'` && \
        bash $$IceCat$$/makeicecat --skip-compare-locales --skip-l10n --no-debian --no-tarball --with-sourcedir=$PWD && \
        bash $$MozLocales$$/prebuild-icecat.sh  && \
        echo "ac_add_options --with-android-ndk=$$NDK$$" >> .mozconfig && \
        echo "ac_add_options --with-android-sdk=$$SDK$$/platforms/android-22" >> .mozconfig && \
        echo "ac_add_options --target=${fxarch}" >> .mozconfig && \
        echo "mk_add_options 'export ANDROID_VERSION_CODE=$$VERCODE$$'" >> .mozconfig
    scanignore=mobile/android/base/JavaAddonManager.java
    build=./mach build && \
        pushd obj-android/mobile/android/locales && \
        for loc in $(cat ../../../../used-locales); do LOCALE_MERGEDIR=$PWD/merge-$loc make merge-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; make LOCALE_MERGEDIR=$PWD/merge-$loc chrome-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; done && \
        popd && \
        ./mach package

Build:38.7.1,380711
    commit=FIREFOX_38_7_1esr_RELEASE
    output=obj-android/dist/gecko-unsigned-unaligned.apk
    srclibs=MozLocales@96b78c77,IceCat@f6c6629a
    prebuild=fxarch=`echo $$VERCODE$$ | cut -c 5 | sed -e 's/0/arm-linux-androideabi/g' -e 's/1/i386-linux-androideabi/g'` && \
        bash $$IceCat$$/makeicecat --skip-compare-locales --skip-l10n --no-debian --no-tarball --with-sourcedir=$PWD && \
        bash $$MozLocales$$/prebuild-icecat.sh  && \
        echo "ac_add_options --with-android-ndk=$$NDK$$" >> .mozconfig && \
        echo "ac_add_options --with-android-sdk=$$SDK$$/platforms/android-22" >> .mozconfig && \
        echo "ac_add_options --target=${fxarch}" >> .mozconfig && \
        echo "mk_add_options 'export ANDROID_VERSION_CODE=$$VERCODE$$'" >> .mozconfig
    scanignore=mobile/android/base/JavaAddonManager.java
    build=./mach build && \
        pushd obj-android/mobile/android/locales && \
        for loc in $(cat ../../../../used-locales); do LOCALE_MERGEDIR=$PWD/merge-$loc make merge-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; make LOCALE_MERGEDIR=$PWD/merge-$loc chrome-$loc LOCALE_MERGEDIR=$PWD/merge-$loc; done && \
        popd && \
        ./mach package

Maintainer Notes:

Dependencies:
 * pip install compare-locales
 * apt-get build-dep firefox
.

Archive Policy:3 versions
Auto Update Mode:None
Update Check Mode:None
Current Version:38.6.0
Current Version Code:380600
