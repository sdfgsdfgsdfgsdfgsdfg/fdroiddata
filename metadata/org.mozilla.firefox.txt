AntiFeatures:NonFreeAdd,Tracking,UpstreamNonFree
Categories:Internet
# mostly...
License:MPL2
Web Site:https://wiki.mozilla.org/Mobile/Platforms/Android#System_Requirements
Source Code:http://hg.mozilla.org
Issue Tracker:https://bugzilla.mozilla.org
Changelog:https://www.mozilla.org/en-US/firefox/releases
Donate:https://sendto.mozilla.org/page/contribute/openwebfund

Name:Firefox
Summary:Web browser
Description:
'''DEPRECATION NOTICE'''

Note that this package will soon be dropped from the F-Droid repo. Please
consider using one of the following options for continued update support:

* [[org.gnu.icecat]] - Free software rebranding of firefox-ESR by GNU
* Manually download latest version from [https://download.mozilla.org/?product=fennec-latest&os=android&lang=multi Mozilla]
* [[de.marmaro.krt.ffupdater]] - external updater for Firefox
* [https://www.mozilla.org/en-US/firefox/channel/#developer Firefox Aurora/Developer Edition] - bleeding edge Firefox with automatic updates
* [[info.guardianproject.orfox]] - based on the TOR Browser bundle of Firefox, get it from the [https://dev.guardianproject.info/debug/info.guardianproject.orfox/fdroid/repo Guardian Project Alpha Channel]

Fennec F-Droid has been moved to the archive repo until it becomes stable and up
to date.

'''Description'''

Mobile version of the Firefox web browser. Uses the Gecko layout engine to
render web pages, which implements current and anticipated web standards.
Features include: bookmark sync, custom search engines, support for addons and
the ability to enable or disable the search suggestions.

'''Anti-Features'''

* Non-free Addons: The license of the addons may be seen in the version notes, but often are non-free. The marketplace has no license information on most apps.
* Tracking: Stats are sent back regularly to the developers, but that can be disabled via settings. Also some versions contain "Adjust" tracking software.
* UpstreamNonFree: Contains proprietary software, e.g. play-services.
.

Maintainer Notes:
Binaries in repo for now, build process is large and complex. The binaries are
very well hidden - Mozilla appear very keen to force use of the proprietary market.
They come from here: https://ftp.mozilla.org/pub/mobile/releases/

The HTTP update check no longer works because they stopped putting any useful
info alongside the binaries.

Starting with 33.0 Firefox contains gms play-services due to chromecasting
capabilities.
.

Auto Update Mode:None
Update Check Mode:None
Current Version:45.0.2
Current Version Code:2015411201
