Categories:Games
License:GPLv2+
Web Site:
Source Code:https://github.com/videogameboy76/frozenbubbleandroid
Issue Tracker:https://github.com/videogameboy76/frozenbubbleandroid/issues

Auto Name:Frozen Bubble
Summary:Bubble shooting game
Description:
A bubble shooting game. Knock the bubbles down by forming clusters of three or
more bubbles.
.

Repo Type:git
Repo:https://github.com/videogameboy76/frozenbubbleandroid

Build:1.7,8
    commit=15

Build:1.8,9
    commit=16

Build:1.10,11
    commit=18

Build:1.11,12
    commit=19

Build:1.12,13
    commit=20

Build:1.13,14
    commit=25
    buildjni=yes

Build:1.14,15
    commit=30
    buildjni=yes

Build:1.15,16
    commit=32
    buildjni=yes

Build:1.16,17
    commit=40
    buildjni=yes

Build:1.17,18
    commit=49
    buildjni=yes

Build:1.18,19
    commit=50
    buildjni=yes

Build:1.19,20
    commit=52
    buildjni=yes

Build:1.20,21
    commit=58
    prebuild=sed -i '156s/true/false/g;448s/true/false/g' src/org/jfedor/frozenbubble/FrozenBubble.java && \
        sed -i '68s/true/false/g;118s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java
    buildjni=yes

Build:1.21,22
    commit=64
    prebuild=sed -i '156s/true/false/g;448s/true/false/g' src/org/jfedor/frozenbubble/FrozenBubble.java && \
        sed -i '68s/true/false/g;118s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java
    buildjni=yes

Build:1.22,23
    commit=65
    prebuild=sed -i '156s/true/false/g;450s/true/false/g' src/org/jfedor/frozenbubble/FrozenBubble.java && \
        sed -i '68s/true/false/g;118s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java
    buildjni=yes

Build:2.1,25
    commit=76
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:2.3,27
    commit=88
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:2.7,31
    commit=93
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.2,35
    commit=100
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.3,36
    commit=101
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.4,37
    commit=103
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.5,38
    commit=109
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.6,39
    commit=eb8033969197077a4f5f08f4522b435d540c770a
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.8,41
    commit=e8ebd0a3bde5cff9fba0b1d43fa802e34e68ec65
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.9,42
    commit=331deaace7d3a6a2a16bfffa06cee99b25a0c0d5
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.10,43
    commit=178b2fe3d4db6f3bd54e1fbfd43ba5ee43717717
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Build:3.11,44
    commit=7af146c925099c92fc7b5888efffd730bc22d161
    prebuild=sed -i '/adsOn/s/true/false/g' src/com/efortin/frozenbubble/PreferencesActivity.java src/org/jfedor/frozenbubble/FrozenBubble.java
    buildjni=yes

Maintainer Notes:
Old Repo for <3.5:https://frozenbubbleandroid.googlecode.com/svn
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.11
Current Version Code:44
